[By tutorial](https://www.youtube.com/watch?v=coQ5dg8wM2o&t=509s)

[Install ganache](https://www.codeooze.com/blockchain/ethereum-dev-environment-2019/)

```
npm install -g truffle
```
Check version
```
truffle --version
```
Output
```
Truffle v5.4.30 - a development framework for Ethereum

Usage: truffle <command> [options]

...
```
Next step
```
truffle init
```

Ganache for launch local ganache (my local path to ganache)
```
~/ForFun/cryptofun/ganache-2.5.4-linux-x86_64.AppImage
```

For launch frontend
```
npm run dev
```
