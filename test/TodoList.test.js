const { assert } = require("chai");

const TodoList = artifacts.require('./TodoList.sol');

contract('TodoList', (accounts) => {
    before(async () => {
        this.todoList = await TodoList.deployed();
    });

    it('deploys successfully', async () => {
        const address = await this.todoList.address;
        assert.notEqual(address, 0x0);
        assert.notEqual(address, '');
        assert.notEqual(address, null);
        assert.notEqual(address, undefined);
    });

    it('lists tasks', async () => {
        const taskCount = await this.todoList.taskCount();
        assert.notEqual(taskCount, 0)

        const task = await this.todoList.tasks(taskCount);
        assert.equal(task.id.toNumber(), taskCount.toNumber());
        assert.equal(task.content, 'Test task');
        assert.equal(task.completed, false);
        assert.equal(taskCount.toNumber(), 1);
    })

    it('create task', async () => {
        const result = await this.todoList.createTask('A new Task');
        const taskCount = await this.todoList.taskCount();
        assert.equal(taskCount, 2)

        const event = result.logs[0].args;
        assert.equal(event.id.toNumber(), 2);
        assert.equal(event.content, 'A new Task');
        assert.equal(event.completed, false);

        const newTask = await this.todoList.tasks(2);
        assert.equal(newTask.id.toNumber(), 2);
        assert.equal(newTask.content, 'A new Task');
        assert.equal(newTask.completed, false);
    })

    it('toggles task completion', async () => {     
        const newTask = await this.todoList.tasks(1);
        assert.equal(newTask.id.toNumber(), 1);
        assert.equal(newTask.content, 'Test task');
        assert.equal(newTask.completed, false);

        const toggle = await this.todoList.toggleCompleted(1);
        const event = toggle.logs[0].args;
        assert.equal(event.id.toNumber(), 1);
        assert.equal(event.content, 'Test task');
        assert.equal(event.completed, true);

        const changedTask = await this.todoList.tasks(1);
        assert.equal(changedTask.id.toNumber(), 1);
        assert.equal(changedTask.content, 'Test task');
        assert.equal(changedTask.completed, true);
    })
})