// SPDX-License-Identifier: MIT
pragma solidity ^0.8.11;

contract TodoList {
    uint public taskCount = 0;

    struct Task {
        uint id;
        string content;
        bool completed;
    }

    mapping(uint => Task) public tasks;

    event TaskCreated(
        uint id,
        string content,
        bool completed
    );

    event TaskChanged(
        uint id,
        string content,
        bool completed
    );

    constructor() {
        createTask("Test task");
    }

    function createTask(string memory _content) public {
        taskCount++;
        tasks[taskCount] = Task({id: taskCount, content: _content, completed: false});
        emit TaskCreated(tasks[taskCount].id, tasks[taskCount].content, tasks[taskCount].completed);
    }

    function toggleCompleted(uint _id) public {
        tasks[_id].completed = !tasks[_id].completed;
        emit TaskChanged(tasks[_id].id, tasks[_id].content, tasks[_id].completed);
    }

}